# Proovitöö

Proovitöö sisendiks on kliendivalim ja Telia raamistiku assets kaust.

Telia koodiraami dokumentatsiooni leiad : http://digi.telia.ee/code/telia-front/master/
Login: telia:veeb

- Loo andmebaas kliendi andmete hoidmiseks;
- Impordi kaustas olev valim andmebaasi;
- Loo võimalus kuvatava kliendi valimiseks/vahetamiseks;
- Loo võimalus kliendi internetimahu ja kõneminutite kasutuse info kuvamiseks;
- Tekita lehele kliendi andmetega eeltäidetud vorm;
- Vormi saatmisel salvesta andmed andmebaasi ja saada email;

Proovitöö esita emaili teel tähtajaks 12.03.2018 kell 17:00.
Emailile lisa proovitöö lähtekood ja andmebaasi dump ja link töötavale lahendusele.

## Vormi väljad

- Nimi;
- Email;
- Telefon;
- Küsimus;

Kui sul on läheülesande kohta küsimusi, siis kirjuta julgelt madis.nikopensius@telia.ee või helista 56 666 931 (ka nädalavahetusel). 
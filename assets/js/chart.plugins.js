(function(root, factory) {
    root['Chartist.plugins.legend'] = factory(root.Chartist);
}(this, function(Chartist) {
    'use strict';

    var defaultOptions = {
        className: '',
        legendNames: false,
        clickable: true
    };

    Chartist.plugins = Chartist.plugins || {};

    /**
     * This Chartist plugin creates a legend to show next to the chart.
     *
     * @param  {Object} options - An option map.
     * @returns {void}
     */
    Chartist.plugins.legend = function(options) {
        options = Chartist.extend({}, defaultOptions, options);

        return function(chart) {
            /**
             * Update chart.
             *
             * @param  {Event} e - Event.
             * @returns {void}
             */
            function legendChildClickEvent(e) {
                e.preventDefault();

                var seriesIndex = parseInt(this.getAttribute('data-legend'));
                var removedSeriesIndex = removedSeries.indexOf(seriesIndex);

                if (removedSeriesIndex > -1) {
                    // Add to series again.
                    removedSeries.splice(removedSeriesIndex, 1);
                    this.classList.remove('inactive');
                } else if (chart.data.series.length > 1) {
                    removedSeries.push(seriesIndex);
                    this.classList.add('inactive');
                }

                // Reset the series to original and remove each series that
                // is still removed again, to remain index order.
                var seriesCopy = originalSeries.slice(0);

                // Reverse sort the removedSeries to prevent removing the wrong index.
                removedSeries.sort().reverse();

                removedSeries.forEach(function(series) {
                    seriesCopy.splice(series, 1);
                });

                chart.data.series = seriesCopy;

                chart.update();
            }

            // Set a unique className for each series so that when a series is removed,
            // the other series still have the same color.
            if (options.clickable) {
                chart.data.series.forEach(function(series, seriesIndex) {
                    if (typeof series !== 'object') {
                        series = {
                            data: series
                        };
                    }

                    series.className = series.className || chart.options.classNames.series + '-' + Chartist.alphaNumerate(seriesIndex);
                });
            }

            var chartElement = chart.container;

            if (!chartElement) {
                return;
            }
            chartElement.innerHTML += '<ul class="ct-legend"></ul>';
            var legendElement = chartElement.querySelector('.ct-legend');

            if (chart instanceof Chartist.Pie) {
                legendElement.classList.add('ct-legend-inside');
            }
            if (typeof options.className === 'string' && options.className.length > 0) {
                legendElement.classList.add(options.className);
            }

            var removedSeries = [];
            var originalSeries = chart.data.series.slice(0);

            // Get the right array to use for generating the legend.
            var legendNames = chart.data.series;

            if (chart instanceof Chartist.Pie) {
                legendNames = chart.data.labels;
            }
            legendNames = options.legendNames || legendNames;

            // Loop through all legends to set each name in a list item.
            var legendHtml = legendNames.map(function(legend, i) {
                var legendName = legend.name || legend;

                return '<li class="ct-series-' + i.toString() + '" data-legend="' + i.toString() + '">' + legendName + '</li>';
            }).join('');

            legendElement.innerHTML = legendHtml;

            if (options.clickable) {
                var legendElementChildren = legendElement.querySelectorAll('li');

                Array.prototype.forEach.call(legendElementChildren, function(legendElementChild) {
                    legendElementChild.onclick = legendChildClickEvent;
                });
            }
        };
    };

    return Chartist.plugins.legend;
}));

/** ***************************************************************************
 *                                                                            *
 *  SVG Path Rounding Function                                                *
 *  Copyright (C) 2014 Yona Appletree                                         *
 *                                                                            *
 *  Licensed under the Apache License, Version 2.0 (the "License");           *
 *  you may not use this file except in compliance with the License.          *
 *  You may obtain a copy of the License at                                   *
 *                                                                            *
 *      http://www.apache.org/licenses/LICENSE-2.0                            *
 *                                                                            *
 *  Unless required by applicable law or agreed to in writing, software       *
 *  distributed under the License is distributed on an "AS IS" BASIS,         *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *
 *  See the License for the specific language governing permissions and       *
 *  limitations under the License.                                            *
 *                                                                            *
 *****************************************************************************/

/**
 * SVG Path rounding function. Takes an input path string and outputs a path
 * string where all line-line corners have been rounded. Only supports absolute
 * commands at the moment.
 *
 * @param {Object} pathString - The SVG input path.
 * @param {number} radius - The amount to round the corners, either a value in the SVG
 *               coordinate space, or, if useFractionalRadius is true, a value
 *               from 0 to 1.
 * @param {boolean} useFractionalRadius - If true, the curve radius is expressed as a
 *               fraction of the distance between the point being curved and
 *               the previous and next points.
 * @param {Array<number>} corners - Array defining what corners to round starting from 1.
 * @returns {Object} A new SVG path string with the rounding.
 */
function roundPathCorners(pathString, radius, useFractionalRadius, corners) {
    /**
     * Move towards length.
     *
     * @param  {number} movingPoint - Moving point.
     * @param  {number} targetPoint - Target point.
     * @param  {number} amount - Amount.
     * @returns {Object} Object of points.
     */
    function moveTowardsLength(movingPoint, targetPoint, amount) {
        var width = (targetPoint.x - movingPoint.x);
        var height = (targetPoint.y - movingPoint.y);

        var distance = Math.sqrt(width * width + height * height);

        return moveTowardsFractional(movingPoint, targetPoint, Math.min(1, amount / distance));
    }

    /**
     * [moveTowardsFractional description].
     *
     * @param  {number} movingPoint - Moving point.
     * @param  {number} targetPoint - Target point.
     * @param  {number} fraction - Fraction.
     * @returns {Object} Object of points.
     */
    function moveTowardsFractional(movingPoint, targetPoint, fraction) {
        return {
            x: movingPoint.x + (targetPoint.x - movingPoint.x) * fraction,
            y: movingPoint.y + (targetPoint.y - movingPoint.y) * fraction
        };
    }

    /**
     * Adjusts the ending position of a command.
     *
     * @param  {number} cmd - Width of cmd.
     * @param  {Object} newPoint - Object of points.
     * @returns {void}
     */
    function adjustCommand(cmd, newPoint) {
        if (cmd.length > 2) {
            cmd[cmd.length - 2] = newPoint.x;
            cmd[cmd.length - 1] = newPoint.y;
        }
    }

    /**
     * Gives an {x, y} object for a command's ending position.
     *
     * @param  {number} cmd - Width of cmd.
     * @returns {Object} Object of points.
     */
    function pointForCommand(cmd) {
        return {
            x: parseFloat(cmd[cmd.length - 2]),
            y: parseFloat(cmd[cmd.length - 1])
        };
    }

    // Split apart the path, handing concatonated letters and numbers
    var pathParts = pathString
        .split(/[,\s]/)
        .reduce(function(parts, part) {
            var match = part.match('([a-zA-Z])(.+)');

            if (match) {
                parts.push(match[1]);
                parts.push(match[2]);
            } else {
                parts.push(part);
            }

            return parts;
        }, []);

    // Group the commands with their arguments for easier handling
    var commands = pathParts.reduce(function(commands, part) {
        if (parseFloat(part) == part && commands.length) {
            commands[commands.length - 1].push(part);
        } else {
            commands.push([part]);
        }

        return commands;
    }, []);

    // The resulting commands, also grouped
    var resultCommands = [];

    if (commands.length > 1) {
        var startPoint = pointForCommand(commands[0]);

        // Handle the close path case with a "virtual" closing line
        var virtualCloseLine = null;

        if (commands[commands.length - 1][0] == 'Z' && commands[0].length > 2) {
            virtualCloseLine = ['L', startPoint.x, startPoint.y];
            commands[commands.length - 1] = virtualCloseLine;
        }

        // We always use the first command (but it may be mutated)
        resultCommands.push(commands[0]);

        for (var cmdIndex = 1; cmdIndex < commands.length; cmdIndex++) {
            var prevCmd = resultCommands[resultCommands.length - 1];

            var curCmd = commands[cmdIndex];

            // Handle closing case
            var nextCmd = (curCmd == virtualCloseLine) ? commands[1] : commands[cmdIndex + 1];

            // Nasty logic to decide if this path is a candidite.
            // Also only round the corners definedin corners array

            if ((corners.indexOf(cmdIndex) !== -1) && nextCmd && prevCmd && (prevCmd.length > 2) && curCmd[0] == 'L' && nextCmd.length > 2 && nextCmd[0] == 'L') {
                // Calc the points we're dealing with
                var prevPoint = pointForCommand(prevCmd);
                var curPoint = pointForCommand(curCmd);
                var nextPoint = pointForCommand(nextCmd);

                // The start and end of the cuve are just our point moved towards the previous and next points, respectivly
                var curveStart;
                var curveEnd;

                curveStart = moveTowardsLength(curPoint, prevPoint, radius);
                curveEnd = moveTowardsLength(curPoint, nextPoint, radius);

                // Adjust the current command and add it
                adjustCommand(curCmd, curveStart);
                curCmd.origPoint = curPoint;
                resultCommands.push(curCmd);

                // The curve control points are halfway between the start/end of the curve and
                // the original point
                var startControl = moveTowardsFractional(curveStart, curPoint, .5);
                var endControl = moveTowardsFractional(curPoint, curveEnd, .5);

                // Create the curve
                var curveCmd = ['C', startControl.x, startControl.y, endControl.x, endControl.y, curveEnd.x, curveEnd.y];
                // Save the original point for fractional calculations

                curveCmd.origPoint = curPoint;
                resultCommands.push(curveCmd);
            } else {
                // Pass through commands that don't qualify
                resultCommands.push(curCmd);
            }
        }

        // Fix up the starting point and restore the close path if the path was orignally closed
        if (virtualCloseLine) {
            var newStartPoint = pointForCommand(resultCommands[resultCommands.length - 1]);

            resultCommands.push(['Z']);
            adjustCommand(resultCommands[0], newStartPoint);
        }
    } else {
        resultCommands = commands;
    }

    return resultCommands.reduce(function(str, c) { return str + c.join(' ') + ' '; }, '');
}

window.roundPathCorners = roundPathCorners;
